Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: oclhashcat
Source: https://hashcat.net/oclhashcat/

Files: *
Copyright: 2015 Jens Steube 
License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Files: include-header/adl*
Copyright: AMD all right reserved
License: other
 See http://developer.amd.com/tools-and-sdks/graphics-development/display-library-adl-sdk/
 Fair use

Files: include-header/nvml.h
Copyright: 1993-2015 NVIDIA Corporation.  All rights reserved.
License: other
 This source code is subject to NVIDIA ownership rights under U.S. and 
 international Copyright laws.  Users and possessors of this source code 
 are hereby granted a nonexclusive, royalty-free license to use this code 
 in individual and commercial software.
 .
 NVIDIA MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE 
 CODE FOR ANY PURPOSE.  IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR 
 IMPLIED WARRANTY OF ANY KIND.  NVIDIA DISCLAIMS ALL WARRANTIES WITH 
 REGARD TO THIS SOURCE CODE, INCLUDING ALL IMPLIED WARRANTIES OF 
 MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE.
 IN NO EVENT SHALL NVIDIA BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, 
 OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS 
 OF USE, DATA OR PROFITS,  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE 
 OR OTHER TORTIOUS ACTION,  ARISING OUT OF OR IN CONNECTION WITH THE USE 
 OR PERFORMANCE OF THIS SOURCE CODE.  
 .
 U.S. Government End Users.   This source code is a "commercial item" as 
 that term is defined at  48 C.F.R. 2.101 (OCT 1995), consisting  of 
 "commercial computer  software"  and "commercial computer software 
 documentation" as such terms are  used in 48 C.F.R. 12.212 (SEPT 1995) 
 and is provided to the U.S. Government only as a commercial end item.  
 Consistent with 48 C.F.R.12.212 and 48 C.F.R. 227.7202-1 through 
 227.7202-4 (JUNE 1995), all U.S. Government End Users acquire the 
 source code with only those rights set forth herein. 
 .
 Any use of this source code in individual and commercial software must 
 include, in the user documentation and internal comments to the code,
 the above Disclaimer and U.S. Government End Users Notice.

Files: debian/*
Copyright: 2013 Devon Kearns <dookie@kali.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
